import { MultiDimensionalChildren } from './multi-dimensional-children.type.js';

export type ResolvedTsxComponent = (
  props: any,
  ...children: MultiDimensionalChildren
) => Promise<string>;
