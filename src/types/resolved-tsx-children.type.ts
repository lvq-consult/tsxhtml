export type ResolvedTsxChild =
  | string
  | string[]
  | Promise<string | string[]>
  | Promise<(string | string[])[]>;

export type ResolvedTsxChildren = ResolvedTsxChild[];
