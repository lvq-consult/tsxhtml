/// <reference lib="DOM" />

// IntrinsicElementMap grabs all the standard HTML tags in the TS DOM lib.
export interface TsxHtmlIntrinsicElements {
  a: TsxHtmlHTMLAnchorElement;
  abbr: TsxHtmlHTMLElement;
  address: TsxHtmlHTMLElement;
  area: TsxHtmlHTMLAreaElement;
  article: TsxHtmlHTMLElement;
  aside: TsxHtmlHTMLElement;
  audio: TsxHtmlHTMLAudioElement;
  b: TsxHtmlHTMLElement;
  bb: TsxHtmlHTMLBrowserButtonElement;
  base: TsxHtmlBaseTag;
  bdi: TsxHtmlHTMLElement;
  bdo: TsxHtmlHTMLElement;
  blockquote: TsxHtmlHTMLQuoteElement;
  body: TsxHtmlHTMLBodyElement;
  br: TsxHtmlHTMLElement;
  button: TsxHtmlHTMLButtonElement;
  canvas: TsxHtmlHTMLCanvasElement;
  caption: TsxHtmlHTMLElement;
  cite: TsxHtmlHTMLElement;
  code: TsxHtmlHTMLElement;
  col: TsxHtmlHTMLTableColElement;
  colgroup: TsxHtmlHTMLTableColElement;
  commands: TsxHtmlHTMLCommandElement;
  data: TsxHtmlDataTag;
  datalist: TsxHtmlHTMLDataListElement;
  dd: TsxHtmlHTMLElement;
  del: TsxHtmlHTMLModElement;
  details: TsxHtmlHTMLDetailsElement;
  dfn: TsxHtmlHTMLElement;
  div: TsxHtmlHTMLElement;
  dl: TsxHtmlHTMLElement;
  dt: TsxHtmlHTMLElement;
  em: TsxHtmlHTMLElement;
  embed: TsxHtmlHTMLEmbedElement;
  fieldset: TsxHtmlHTMLFieldSetElement;
  figcaption: TsxHtmlHTMLElement;
  figure: TsxHtmlHTMLElement;
  footer: TsxHtmlHTMLElement;
  form: TsxHtmlHTMLFormElement;
  h1: TsxHtmlHTMLElement;
  h2: TsxHtmlHTMLElement;
  h3: TsxHtmlHTMLElement;
  h4: TsxHtmlHTMLElement;
  h5: TsxHtmlHTMLElement;
  h6: TsxHtmlHTMLElement;
  head: TsxHtmlHTMLElement;
  header: TsxHtmlHTMLElement;
  hr: TsxHtmlHTMLElement;
  html: TsxHtmlHTMLHtmlElement;
  i: TsxHtmlHTMLElement;
  iframe: TsxHtmlHTMLIFrameElement;
  img: TsxHtmlHTMLImageElement;
  input: TsxHtmlHTMLInputElement;
  ins: TsxHtmlHTMLModElement;
  kbd: TsxHtmlHTMLElement;
  keygen: TsxHtmlKeygenTag;
  label: TsxHtmlHTMLLabelElement;
  legend: TsxHtmlHTMLLegendElement;
  li: TsxHtmlHTMLLIElement;
  link: TsxHtmlHTMLLinkElement;
  main: TsxHtmlHTMLElement;
  map: TsxHtmlHTMLMapElement;
  mark: TsxHtmlHTMLElement;
  menu: TsxHtmlHTMLMenuElement;
  meta: TsxHtmlHTMLMetaElement;
  meter: TsxHtmlHTMLMeterElement;
  nav: TsxHtmlHTMLElement;
  noscript: TsxHtmlHTMLElement;
  object: TsxHtmlHTMLObjectElement;
  ol: TsxHtmlHTMLOListElement;
  optgroup: TsxHtmlHTMLOptgroupElement;
  option: TsxHtmlHTMLOptionElement;
  output: TsxHtmlHTMLOutputElement;
  p: TsxHtmlHTMLElement;
  param: TsxHtmlHTMLParamElement;
  pre: TsxHtmlHTMLElement;
  progress: TsxHtmlHTMLProgressElement;
  q: TsxHtmlHTMLQuoteElement;
  rb: TsxHtmlHTMLElement;
  rp: TsxHtmlHTMLElement;
  rt: TsxHtmlHTMLElement;
  rtc: TsxHtmlHTMLElement;
  ruby: TsxHtmlHTMLElement;
  s: TsxHtmlHTMLElement;
  samp: TsxHtmlHTMLElement;
  script: TsxHtmlHTMLScriptElement;
  section: TsxHtmlHTMLElement;
  select: TsxHtmlHTMLSelectElement;
  small: TsxHtmlHTMLElement;
  source: TsxHtmlHTMLSourceElement;
  span: TsxHtmlHTMLElement;
  strong: TsxHtmlHTMLElement;
  style: TsxHtmlHTMLStyleElement;
  sub: TsxHtmlHTMLElement;
  sup: TsxHtmlHTMLElement;
  table: TsxHtmlHTMLTableElement;
  tbody: TsxHtmlHTMLElement;
  td: TsxHtmlHTMLTableDataCellElement;
  template: TsxHtmlHTMLElement;
  textarea: TsxHtmlHTMLTextAreaElement;
  tfoot: TsxHtmlHTMLTableSectionElement;
  th: TsxHtmlHTMLTableHeaderCellElement;
  thead: TsxHtmlHTMLTableSectionElement;
  time: TsxHtmlHTMLTimeElement;
  title: TsxHtmlHTMLElement;
  tr: TsxHtmlHTMLTableRowElement;
  track: TsxHtmlHTMLTrackElement;
  u: TsxHtmlHTMLElement;
  ul: TsxHtmlHTMLElement;
  var: TsxHtmlHTMLElement;
  video: TsxHtmlHTMLVideoElement;
  wbr: TsxHtmlHTMLElement;
  '!doctype': any;

  // SVG
  animate: any;
  animateMotion: any;
  animateTransform: any;
  circle: any;
  clipPath: any;
  defs: any;
  desc: any;
  ellipse: any;
  feBlend: any;
  feColorMatrix: any;
  feComponentTransfer: any;
  feComposite: any;
  feConvolveMatrix: any;
  feDiffuseLighting: any;
  feDisplacementMap: any;
  feDistantLight: any;
  feDropShadow: any;
  feFlood: any;
  feFuncA: any;
  feFuncB: any;
  feFuncG: any;
  feFuncR: any;
  feGaussianBlur: any;
  feImage: any;
  feMerge: any;
  feMergeNode: any;
  feMorphology: any;
  feOffset: any;
  fePointLight: any;
  feSpecularLighting: any;
  feSpotLight: any;
  feTile: any;
  feTurbulence: any;
  filter: any;
  foreignObject: any;
  g: any;
  hatch: any;
  hatchpath: any;
  image: any;
  line: any;
  linearGradient: any;
  marker: any;
  mask: any;
  metadata: any;
  mpath: any;
  path: any;
  pattern: any;
  polygon: any;
  polyline: any;
  radialGradient: any;
  rect: any;
  set: any;
  stop: any;
  svg: any;
  switch: any;
  symbol: any;
  text: any;
  textPath: any;
  tspan: any;
  use: any;
  view: any;
}

export interface TsxHtmlHTMLElement {
  accesskey?: string;
  class?: string;
  contenteditable?: string;
  dir?: string;
  hidden?: string | boolean;
  id?: string;
  role?: string;
  lang?: string;
  draggable?: string | boolean;
  spellcheck?: string | boolean;
  style?: string;
  tabindex?: string;
  title?: string;
  translate?: string | boolean;
}

export interface TsxHtmlHTMLAnchorElement extends TsxHtmlHTMLElement {
  href?: string;
  target?: string;
  download?: string;
  ping?: string;
  rel?: string;
  media?: string;
  hreflang?: string;
  type?: string;
}

export interface TsxHtmlHTMLAreaElement extends TsxHtmlHTMLElement {
  alt?: string;
  coords?: string;
  shape?: string;
  href?: string;
  target?: string;
  ping?: string;
  rel?: string;
  media?: string;
  hreflang?: string;
  type?: string;
}

export interface TsxHtmlHTMLAudioElement extends TsxHtmlHTMLElement {
  src?: string;
  autobuffer?: string;
  autoplay?: string;
  loop?: string;
  controls?: string;
}

export interface TsxHtmlBaseTag extends TsxHtmlHTMLElement {
  href?: string;
  target?: string;
}

export interface TsxHtmlHTMLQuoteElement extends TsxHtmlHTMLElement {
  cite?: string;
}

export interface TsxHtmlHTMLBodyElement extends TsxHtmlHTMLElement {}

export interface TsxHtmlHTMLButtonElement extends TsxHtmlHTMLElement {
  action?: string;
  autofocus?: string;
  disabled?: string;
  enctype?: string;
  form?: string;
  method?: string;
  name?: string;
  novalidate?: string | boolean;
  target?: string;
  type?: string;
  value?: string;
}

export interface TsxHtmlHTMLDataListElement extends TsxHtmlHTMLElement {}
export interface TsxHtmlHTMLCanvasElement extends TsxHtmlHTMLElement {
  width?: string;
  height?: string;
}

export interface TsxHtmlHTMLTableColElement extends TsxHtmlHTMLElement {
  span?: string;
}

export interface TsxHtmlHTMLTableSectionElement extends TsxHtmlHTMLElement {}

export interface TsxHtmlHTMLTableRowElement extends TsxHtmlHTMLElement {}

export interface TsxHtmlDataTag extends TsxHtmlHTMLElement {
  value?: string;
}

export interface TsxHtmlHTMLEmbedElement extends TsxHtmlHTMLElement {
  src?: string;
  type?: string;
  width?: string;
  height?: string;
  [anything: string]: string | boolean | undefined;
}

export interface TsxHtmlHTMLFieldSetElement extends TsxHtmlHTMLElement {
  disabled?: string;
  form?: string;
  name?: string;
}

export interface TsxHtmlHTMLFormElement extends TsxHtmlHTMLElement {
  acceptCharset?: string;
  action?: string;
  autocomplete?: string;
  enctype?: string;
  method?: string;
  name?: string;
  novalidate?: string | boolean;
  target?: string;
}

export interface TsxHtmlHTMLHtmlElement extends TsxHtmlHTMLElement {
  manifest?: string;
}

export interface TsxHtmlHTMLIFrameElement extends TsxHtmlHTMLElement {
  src?: string;
  srcdoc?: string;
  name?: string;
  sandbox?: string;
  seamless?: string;
  width?: string;
  height?: string;
}

export interface TsxHtmlHTMLImageElement extends TsxHtmlHTMLElement {
  alt?: string;
  src?: string;
  crossorigin?: string;
  usemap?: string;
  ismap?: string;
  width?: string;
  height?: string;
}

export interface TsxHtmlHTMLInputElement extends TsxHtmlHTMLElement {
  accept?: string;
  action?: string;
  alt?: string;
  autocomplete?: string;
  autofocus?: string;
  checked?: string | boolean;
  disabled?: string | boolean;
  enctype?: string;
  form?: string;
  height?: string;
  list?: string;
  max?: string;
  maxlength?: string;
  method?: string;
  min?: string;
  multiple?: string;
  name?: string;
  novalidate?: string | boolean;
  pattern?: string;
  placeholder?: string;
  readonly?: string;
  required?: string | boolean;
  size?: string;
  src?: string;
  step?: string;
  target?: string;
  type?: string;
  value?: string;
  width?: string;
}

export interface TsxHtmlHTMLModElement extends TsxHtmlHTMLElement {
  cite?: string;
  datetime?: string | Date;
}

export interface TsxHtmlKeygenTag extends TsxHtmlHTMLElement {
  autofocus?: string;
  challenge?: string;
  disabled?: string;
  form?: string;
  keytype?: string;
  name?: string;
}

export interface TsxHtmlHTMLLabelElement extends TsxHtmlHTMLElement {
  form?: string;
  for?: string;
}

export interface TsxHtmlHTMLLIElement extends TsxHtmlHTMLElement {
  value?: string | number;
}

export interface TsxHtmlHTMLLinkElement extends TsxHtmlHTMLElement {
  href?: string;
  crossorigin?: string;
  rel?: string;
  media?: string;
  hreflang?: string;
  type?: string;
  sizes?: string;
  integrity?: string;
}

export interface TsxHtmlHTMLMapElement extends TsxHtmlHTMLElement {
  name?: string;
}

export interface TsxHtmlHTMLMetaElement extends TsxHtmlHTMLElement {
  name?: string;
  httpEquiv?: string;
  content?: string;
  charset?: string;
  property?: string;
}

export interface TsxHtmlHTMLMeterElement extends TsxHtmlHTMLElement {
  value?: string | number;
  min?: string | number;
  max?: string | number;
  low?: string | number;
  high?: string | number;
  optimum?: string | number;
}

export interface TsxHtmlHTMLObjectElement extends TsxHtmlHTMLElement {
  data?: string;
  type?: string;
  name?: string;
  usemap?: string;
  form?: string;
  width?: string;
  height?: string;
}

export interface TsxHtmlHTMLOListElement extends TsxHtmlHTMLElement {
  reversed?: string;
  start?: string | number;
}

export interface TsxHtmlHTMLOptgroupElement extends TsxHtmlHTMLElement {
  disabled?: string;
  label?: string;
}

export interface TsxHtmlHTMLOptionElement extends TsxHtmlHTMLElement {
  disabled?: string;
  label?: string;
  selected?: string;
  value?: string;
}

export interface TsxHtmlHTMLOutputElement extends TsxHtmlHTMLElement {
  for?: string;
  form?: string;
  name?: string;
}

export interface TsxHtmlHTMLParamElement extends TsxHtmlHTMLElement {
  name?: string;
  value?: string;
}

export interface TsxHtmlHTMLProgressElement extends TsxHtmlHTMLElement {
  value?: string | number;
  max?: string | number;
}

export interface TsxHtmlHTMLCommandElement extends TsxHtmlHTMLElement {
  type?: string;
  label?: string;
  icon?: string;
  disabled?: string;
  checked?: string;
  radiogroup?: string;
  default?: string;
}

export interface TsxHtmlHTMLLegendElement extends TsxHtmlHTMLElement {}

export interface TsxHtmlHTMLBrowserButtonElement extends TsxHtmlHTMLElement {
  type?: string;
}

export interface TsxHtmlHTMLMenuElement extends TsxHtmlHTMLElement {
  type?: string;
  label?: string;
}

type ScriptTagTypeAttribute =
  | 'module'
  | 'text/javascript'
  | 'importmap'
  | string;

type ScriptTagCroosoriginAttribute = 'anonymous' | 'use-credentials';

type ScriptTagFetchPriorityAttribute = 'high' | 'low' | 'auto';

type ScriptTagReferrerPolicyAttribute =
  | 'no-referrer'
  | 'no-referrer-when-downgrade'
  | 'origin'
  | 'origin-when-cross-origin'
  | 'same-origin'
  | 'strict-origin'
  | 'strict-origin-when-cross-origin'
  | 'unsafe-url';

export interface TsxHtmlHTMLScriptElement extends TsxHtmlHTMLElement {
  src?: string;
  charset?: string;
  text?: string;
  type?: ScriptTagTypeAttribute;
  id?: string;
  async?: boolean;
  defer?: boolean;
  crossorigin?: ScriptTagCroosoriginAttribute;
  integrity?: string;
  fetchpriority?: ScriptTagFetchPriorityAttribute;
  nomodule?: boolean;
  blocking?: 'render';
  ScriptTagReferrerPolicyAttribute?: ScriptTagReferrerPolicyAttribute;
}

export interface TsxHtmlHTMLDetailsElement extends TsxHtmlHTMLElement {
  open?: string;
}

export interface TsxHtmlHTMLSelectElement extends TsxHtmlHTMLElement {
  autofocus?: string;
  disabled?: string;
  form?: string;
  multiple?: string;
  name?: string;
  required?: string;
  size?: string;
}

export interface TsxHtmlHTMLSourceElement extends TsxHtmlHTMLElement {
  src?: string;
  type?: string;
  media?: string;
}

export interface TsxHtmlHTMLStyleElement extends TsxHtmlHTMLElement {
  media?: string;
  type?: string;
  disabled?: string;
  scoped?: string;
}

export interface TsxHtmlHTMLTableElement extends TsxHtmlHTMLElement {}

export interface TsxHtmlHTMLTableDataCellElement extends TsxHtmlHTMLElement {
  colspan?: string | number;
  rowspan?: string | number;
  headers?: string;
}

export interface TsxHtmlHTMLTextAreaElement extends TsxHtmlHTMLElement {
  autofocus?: string;
  cols?: string;
  dirname?: string;
  disabled?: string;
  form?: string;
  maxlength?: string;
  minlength?: string;
  name?: string;
  placeholder?: string;
  readonly?: string;
  required?: string;
  rows?: string;
  wrap?: string;
}

export interface TsxHtmlHTMLTableHeaderCellElement extends TsxHtmlHTMLElement {
  colspan?: string | number;
  rowspan?: string | number;
  headers?: string;
  scope?: string;
}

export interface TsxHtmlHTMLTimeElement extends TsxHtmlHTMLElement {
  datetime?: string | Date;
}

export interface TsxHtmlHTMLTrackElement extends TsxHtmlHTMLElement {
  default?: string;
  kind?: string;
  label?: string;
  src?: string;
  srclang?: string;
}

export interface TsxHtmlHTMLVideoElement extends TsxHtmlHTMLElement {
  src?: string;
  poster?: string;
  autobuffer?: string;
  autoplay?: string;
  loop?: string;
  controls?: string;
  width?: string;
  height?: string;
}
