export type UnresolvedTsxChildren = (
  | string
  | Promise<string>
  | Promise<UnresolvedTsxChildren>
  | Promise<UnresolvedTsxChildren[]>
  | UnresolvedTsxChildren
  | UnresolvedTsxChildren[]
)[];
