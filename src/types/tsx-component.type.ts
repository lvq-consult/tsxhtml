import { TSXElement } from './tsx-element.type.js';
import { UnresolvedTsxChildren } from './unresolved-tsx-children.type.js';

export type TsxComponent = (
  props: any,
  ...children: UnresolvedTsxChildren
) => Promise<TSXElement>;
