export type TSXElement = string | Promise<string> | Promise<TSXElement>;
