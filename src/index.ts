import { renderTsx } from './render-tsx.fn.js';
import { renderTsxFragment } from './render-tsx-fragment.fn.js';
import { TSXElement } from './types/tsx-element.type.js';

export * from './types/intrinsic-elements.type.js';
export * from './types/tsx-element.type.js';

declare global {
  function tsxHtml(props: any, ...children: any[]): Promise<TSXElement>;

  function tsxFragmentHtml(
    tagOrComp: TSXElement,
    props: any,
    ...children: any[]
  ): Promise<TSXElement>;
}

globalThis.tsxHtml = renderTsx;
globalThis.tsxFragmentHtml = renderTsxFragment;
