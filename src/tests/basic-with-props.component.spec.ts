import '../index.js';

import test from 'ava';

import { BasicWithPropsComponent } from './basic-with-props.component.js';

test('can render', async t => {
  const result = await BasicWithPropsComponent({ input: 'hello world' });

  t.is(result, '<div>hello world</div>');
});
