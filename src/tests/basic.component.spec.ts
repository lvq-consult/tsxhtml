import '../index.js';

import test from 'ava';

import { BasicComponent } from './basic.component.js';

test('can render', async t => {
  const result = await BasicComponent();

  t.is(result, '<div></div>');
});
