export const FragmentWithChildrenComponent = (
  { title }: { title: string },
  ...children: any[]
) => (
  <>
    <h1>{title}</h1>
    <div>{...children}</div>
  </>
);
