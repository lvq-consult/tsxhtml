import '../index.js';

import test from 'ava';

import { BasicWithPropsComponent } from './basic-with-props.component.js';
import { FragmentWithChildrenComponent } from './fragment-with-children.component.js';

test('can render', async t => {
  const result = await FragmentWithChildrenComponent(
    { title: 'hello title' },
    BasicWithPropsComponent({ input: 'hello world' }),
    BasicWithPropsComponent({ input: 'hello world' }),
  );

  t.is(
    result,
    '<h1>hello title</h1><div><div>hello world</div><div>hello world</div></div>',
  );
});
