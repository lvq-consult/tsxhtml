import '../index.js';

import test from 'ava';

import { BasicWithPropsComponent } from './basic-with-props.component.js';
import { HasChildrenComponent } from './has-children.component.js';

test('can render', async t => {
  const result = await HasChildrenComponent(
    BasicWithPropsComponent({ input: 'hello world' }),
    BasicWithPropsComponent({ input: 'hello world' }),
  );

  t.is(result, '<div><div>hello world</div><div>hello world</div></div>');
});
