export const HasChildrenComponent = (...children: any[]) => (
  <div>{...children}</div>
);
