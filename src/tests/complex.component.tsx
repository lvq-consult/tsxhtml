export const ComplexComponent = (
  { title }: { title: string },
  ...children: any[]
) => (
  <div>
    <h1>{title}</h1>
    <hr class="some-class" />
    <div>{...children}</div>
    <input type="text" required={true} />
  </div>
);
