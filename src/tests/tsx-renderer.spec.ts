import '../index.js';

import test from 'ava';

import { renderTsxFragment } from '../render-tsx-fragment.fn.js';
import { TsxRenderer } from '../tsx-renderer.js';
import { BasicComponent } from './basic.component.js';

test('Has instance', t => {
  const instance = TsxRenderer.instance;

  t.not(instance, undefined);
});

test('render() can render div tag', async t => {
  const result = await TsxRenderer.instance.render('div', {});

  t.is(result, '<div></div>');
});

test('render() can render div tag with attributes', async t => {
  const result = await TsxRenderer.instance.render('div', {
    class: 'some css classes',
  });

  t.is(result, '<div class="some css classes"></div>');
});

test('render() can take a function', async t => {
  const result = await TsxRenderer.instance.render(
    renderTsxFragment,
    {
      class: 'some css classes',
    },
    BasicComponent(),
    BasicComponent(),
  );

  t.is(result, '<div></div><div></div>');
});

test('render() throws if not string or function', async t => {
  await t.throwsAsync(() =>
    TsxRenderer.instance.render(undefined as unknown as string, {
      class: 'some css classes',
    }),
  );
});

test('render() can not render children for self closing tags', async t => {
  await t.throwsAsync(() =>
    TsxRenderer.instance.render(
      'input',
      {
        class: 'some css classes',
      },
      BasicComponent(),
      BasicComponent(),
    ),
  );
});

test('render() can handle multidimensional children', async t => {
  const result = await TsxRenderer.instance.render(
    renderTsxFragment,
    {
      class: 'some css classes',
    },
    [BasicComponent(), BasicComponent()],
  );

  t.is(result, '<div></div><div></div>');
});
