import '../index.js';

import test from 'ava';

import { BasicWithPropsComponent } from './basic-with-props.component.js';
import { ComplexComponent } from './complex.component.js';

test('can render', async t => {
  const result = await ComplexComponent(
    { title: 'Title' },
    BasicWithPropsComponent({ input: 'Child' }),
    BasicWithPropsComponent({ input: 'Child' }),
    BasicWithPropsComponent({ input: 'Child' }),
  );

  t.is(
    result,
    '<div><h1>Title</h1><hr class="some-class" /><div><div>Child</div><div>Child</div><div>Child</div></div><input type="text" required /></div>',
  );
});
