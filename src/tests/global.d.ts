import { TsxHtmlIntrinsicElements } from '../types/intrinsic-elements.type.js';

declare global {
  namespace JSX {
    interface IntrinsicElements extends TsxHtmlIntrinsicElements {}
  }
}
