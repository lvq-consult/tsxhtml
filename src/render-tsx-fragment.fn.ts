import { TsxRenderer } from './tsx-renderer.js';
import { UnresolvedTsxChildren } from './types/unresolved-tsx-children.type.js';

export async function renderTsxFragment<TProps>(
  props: TProps,
  ...children: UnresolvedTsxChildren
): Promise<string> {
  const result = await TsxRenderer.instance.render('', props, ...children);

  return result;
}
