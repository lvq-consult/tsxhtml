import { TsxRenderer } from './tsx-renderer.js';
import { TSXElement } from './types/tsx-element.type.js';
import { UnresolvedTsxChildren } from './types/unresolved-tsx-children.type.js';

export async function renderTsx<TProps>(
  tagOrComp: TSXElement,
  props: TProps,
  ...children: UnresolvedTsxChildren
): Promise<string> {
  const result = await TsxRenderer.instance.render(
    tagOrComp,
    props,
    ...children,
  );

  return result;
}
