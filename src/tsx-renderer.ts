import { MultiDimensionalChildren } from './types/multi-dimensional-children.type.js';
import { ResolvedTsxComponent } from './types/resolved-tsx-component.type.js';
import { TSXElement } from './types/tsx-element.type.js';
import { UnresolvedTsxChildren } from './types/unresolved-tsx-children.type.js';

export class TsxRenderer {
  private static _instance?: TsxRenderer;

  public static BOOLEAN_ATTRIBUTES: string[] = [
    'allowfullscreen',
    'async',
    'autofocus',
    'autoplay',
    'checked',
    'controls',
    'default',
    'defer',
    'disabled',
    'formnovalidate',
    'inert',
    'ismap',
    'itemscope',
    'loop',
    'multiple',
    'muted',
    'nomodule',
    'novalidate',
    'open',
    'playsinline',
    'readonly',
    'required',
    'reversed',
    'selected',
  ];

  public static SELF_CLOSING_TAGS: string[] = [
    'area',
    'base',
    'br',
    'col',
    'embed',
    'hr',
    'img',
    'input',
    'link',
    'meta',
    'param',
    'source',
    'track',
    'wbr',
  ];

  public static get instance() {
    return (this._instance ??= new TsxRenderer());
  }

  async render<TProps>(
    tagOrComp:
      | TSXElement
      | (<TProps>(
          props: TProps,
          ...children: UnresolvedTsxChildren
        ) => Promise<TSXElement>),
    props: TProps,
    ...children: UnresolvedTsxChildren
  ): Promise<string> {
    const childrenLength = children?.length ?? 0;
    const resolvedChildren = childrenLength
      ? await this.awaitChildren(children)
      : [];

    if (typeof tagOrComp === 'string') {
      const tag = await this.renderTag(tagOrComp, props, ...resolvedChildren);

      return tag;
    }

    if (typeof tagOrComp === 'function') {
      const component = await this.renderComponent(
        tagOrComp as ResolvedTsxComponent,
        props,
        ...resolvedChildren,
      );

      return component;
    }

    throw new Error('unknown component');
  }

  private async renderTag<TProps>(
    tag: string,
    props: TProps,
    ...children: MultiDimensionalChildren
  ) {
    const t = tag.trimStart().trimEnd();
    const attributes = Object.keys(props ?? {})
      .filter(key => this.filterAttribute(key, props))
      .map(key => this.renderAttribute(key, props[key]))
      .join(' ');
    const attributesFragment = (
      attributes.length ? ` ${attributes}` : ''
    ).trimEnd();
    const childContent = this.concatenateChildren(children) ?? '';

    if (t === '') {
      return childContent;
    }

    if (childContent && TsxRenderer.SELF_CLOSING_TAGS.includes(t)) {
      throw new Error(
        `Unable to render child content for <${t} /> it's self-closing`,
      );
    }

    if (TsxRenderer.SELF_CLOSING_TAGS.includes(t)) {
      const element = `<${t}${attributesFragment} />`;

      return element;
    } else {
      const element = `<${t}${attributesFragment}>${childContent}</${t}>`;

      return element;
    }
  }

  private async renderComponent<TProps>(
    componentFn: ResolvedTsxComponent,
    props: TProps,
    ...children: MultiDimensionalChildren
  ) {
    const component = await componentFn(props, ...children);

    return component;
  }

  private filterAttribute<TProps>(key: any, props: TProps) {
    const value = props[key];

    if (TsxRenderer.BOOLEAN_ATTRIBUTES.includes(key)) {
      return Boolean(value);
    }

    return true;
  }

  private renderAttribute(key: string, value: string) {
    const attr = TsxRenderer.BOOLEAN_ATTRIBUTES.includes(key)
      ? `${key}`
      : value === undefined
        ? ''
        : `${key}="${value}"`;

    return attr?.trim();
  }

  private async awaitChildren(
    children: UnresolvedTsxChildren | UnresolvedTsxChildren[number],
  ): Promise<MultiDimensionalChildren> {
    const result: MultiDimensionalChildren = [];

    if (children instanceof Promise) {
      const resolvedChild = await children;

      if (typeof resolvedChild === 'string') {
        result.push(resolvedChild);
      } else {
        result.push(await this.awaitChildren(resolvedChild));
      }
    } else if (typeof children === 'string') {
      result.push(children);
    } else if (Array.isArray(children)) {
      const resolvedChild = await Promise.all(
        children.map(child => this.awaitChildren(child)),
      );

      result.push(resolvedChild);
    }

    return result;
  }

  private concatenateChildren(children: MultiDimensionalChildren): string {
    return children.reduce<string>(
      (result, child) =>
        `${result}${
          Array.isArray(child) ? this.concatenateChildren(child) : child
        }`,
      '',
    );
  }
}
